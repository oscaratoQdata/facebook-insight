"""This module is the main transform function for FaceBook's API """

def process(data, config):
  """
  This method do the transformation process for the FaceBook API response
  """

  print('Init facebook transform function')
  print(config)
  import json

  json_data = json.loads(data)
  return json_data
