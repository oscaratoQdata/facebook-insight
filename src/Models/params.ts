export enum methodType {
    Get = "GET",
    Post = "POST"
}

export interface FaceBookParams {
    start_date? : string,
    end_date? : string,
    credentials : {
        access_token: string
    },
    endpoint: {
        start_url: string,
        api_version: string,
        end_url: string,
        campaign_id: string
    },
    parameters: any,
    ds_name? : string,
    config? : any,
    run_id? : string
}