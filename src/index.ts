import {FaceBookHelper} from './facebook_helper';
import {FaceBookParams} from './Models/params';
import * as payloadHelper from './payload_helper';

/**
* Main function for lambda instace
* @param {FaceBookParams} event Evento
* @return {Promise} Retorna una promesa
*/
export function handler(event: FaceBookParams) {
  console.log('Init Lambda execution!!!');
  const faceBookHelper: FaceBookHelper = new FaceBookHelper();
  const config = JSON.parse(event.config);

  const eventConf : any = {
    ds_name: event.ds_name,
    current_step: 'init',
    config: config,
    run_id: event.run_id,
    data: '',
  };

  return new Promise((resolve, reject) => {
    faceBookHelper.processEvent(event)
      .then((result) => {
        console.log('processEvent OK : ', JSON.stringify(result));

        eventConf.data = JSON.stringify([{report: result}]);
        payloadHelper.sendPayload(eventConf, function(error) {
          if (error) reject(error);
          else resolve(result);
        });
      })
      .catch((error) => {
        console.log('processEvent ERROR : ', JSON.stringify(error));
        reject(error);
      });
  });
};
