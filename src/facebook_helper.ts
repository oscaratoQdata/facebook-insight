import {FaceBookParams} from './Models/params';
const request = require('request');

export class FaceBookHelper {
  /**
   * Constructor
   */
  constructor() { }

  /**
   * Method used for call the FaceBook API
   * @param {FaceBookParams} event Required parameters to process the event
   * @return {Promise<FaceBook.ResponseData>} Returns a promise
   */
  processEvent(event: FaceBookParams) {
    return new Promise((resolve, reject) => {
      try {
        let endPoint; let responseJSON;
        
        /**
        * Create the URL of the endPoint
        */
        endPoint = event.endpoint.start_url + '/' + event.endpoint.api_version + '/' +
        event.endpoint.campaign_id + '/' + event.endpoint.end_url;
        endPoint += '?access_token=' + event.credentials.access_token;
        
        /**
         * Create the aditional parameters of the endPoint
         */
        
        if (event.parameters) {
          for (let i = 0; i < event.parameters.length; i++)
            endPoint += '&' + event.parameters[i].name + '=' + event.parameters[i].value;
        }
        console.log(endPoint); // Full URL of endPoint with all parameters
        /**
         * Execute query to endPoint
         */
        request.get(endPoint, function (err: any, response: any, body: string) {
          /**
             * Error in request
             */
          if (err)
            reject('\nError created by request:\n'+err+'\n'+err.toString());
          
          /**
             * Error in API
             */
          responseJSON = JSON.parse(body);
          if (responseJSON['error']) {
            reject('\nError returned by API: \n'+responseJSON['error']['message']);
          } else {
            if (responseJSON['data'])
              resolve(responseJSON['data']);
          }
    
          resolve(responseJSON);
        });
      } catch (error) {
        reject('FaceBookHelper:processEvent [ERROR] :' + error);
      }
    });
  };
}