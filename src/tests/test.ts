// IMPORTS
import * as lambda from '../index';
import {FaceBookParams} from '../Models/params';

//const datasource = require('../../assets/datasource.json');
const datasource = require('../../assets/test_request.json');
const event: FaceBookParams = datasource;

lambda.handler(event)
  .then(() => console.log('done'))
  .catch((error) => console.log('error', JSON.stringify(error)));


